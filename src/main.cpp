#define DODEBUG

#ifdef DODEBUG
#define debug(smth) (Serial.print(smth))
#define debugln(smth) (Serial.println(smth))
#else
#define debug(smth) (NULL)
#define debugln(smth) (NULL)
#endif

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <DNSServer.h>
#include <WiFiManager.h>

#include <mdns.h>

#include <ESP8266HTTPClient.h>

#include <SHT21.h>

#include <Wire.h>
#include <Adafruit_BMP085.h>

#include <StreamString.h>

#define STATION_TIMEOUT 150
#define TIMEOUT 300
#define PROCESS_TIMEOUT 30
#define QUESTION_SERVICE "_meteo._tcp.local"

Sht21 sht21;
Adafruit_BMP085 bmp;

#define MAX_MDNS_PACKET_SIZE 512
byte buffer[MAX_MDNS_PACKET_SIZE];

struct Meteo {
  float temperature = 0, humidity = 0, pressure = 0;
};

struct Service {
  String service = "";
  String host = "";
  String address = "";
  int port = 0;
};

Service service;
Meteo meteo;

bool request(Service service, Meteo meteo, int &timeout) {
  HTTPClient http;
  WiFiClient client;

  const char *url = "/indoor";

  if (!http.begin(client, service.address, service.port, url)) {
    return false;
  }

  http.useHTTP10();
  http.addHeader("Host", service.host, true);
  http.addHeader("Content-Type", "application/json");

  char buf[100];
  sprintf(buf, "{\"temperature\":%0.2f,\"humidity\":%0.2f,\"pressure\":%0.2f}", meteo.temperature, meteo.humidity, meteo.pressure);

  debug("Sending http request to ");
  debug(service.address);
  debug(":");
  debug(service.port);
  debug(url);
  debug(" with content ");
  debugln(buf);

  int httpCode = http.POST(buf);

  debug("Received code ");
  debugln(httpCode);

  // if (httpCode == HTTP_CODE_OK) {
  //   // implementation of http.getString
  //   // original causes exception
  //   // String payload = http.getString();

  //   String s;
  //   StreamString *payload = new StreamString(s);
  //   http.writeToStream(payload);

  //   // debug("Received payload ");
  //   // debugln((String) payload);
  //   int _timeout = payload->toInt();
  //   if (_timeout > 0) {
  //     timeout = _timeout;
  //     debug("Set timeout value to ");
  //     debugln(timeout);
  //   }
  //   delete payload;
  // }

  http.end();

  return httpCode == HTTP_CODE_OK;
}

void answerCallback(const mdns::Answer* answer) {
  debug("Received message ");
  debugln(answer->name_buffer);

  if (answer->rrtype == MDNS_TYPE_PTR and
      strstr(answer->name_buffer, QUESTION_SERVICE) != 0 &&
      service.service == "") {
    service.service = answer->rdata_buffer;
    debug("Service set to ");
    debugln(service.service);
  }

  if (answer->rrtype == MDNS_TYPE_SRV &&
      service.service == answer->name_buffer &&
      service.host == "") {
    char* port_start = strstr(answer->rdata_buffer, "port=");

    if (port_start) {
      port_start += 5;
      char* port_end = strchr(port_start, ';');
      char _port[1 + port_end - port_start];
      strncpy(_port, port_start, port_end - port_start);
      _port[port_end - port_start] = '\0';

      if (port_end) {
        char* host_start = strstr(port_end, "host=");
        if (host_start) {
          host_start += 5;
          service.host = host_start;
          debug("Host set to ");
          debugln(service.host);
          service.port = atoi(_port);
          debug("Port set to ");
          debugln(service.port);
        }
      }
    }
  }

  if (answer->rrtype == MDNS_TYPE_A &&
      service.host == answer->name_buffer &&
      service.address == "") {
    service.address = answer->rdata_buffer;
    debug("Address set to ");
    debugln(service.address);
  }
}

mdns::MDns m_mdns(NULL, NULL, answerCallback, buffer, MAX_MDNS_PACKET_SIZE);

void setup() {
#ifdef DODEBUG
  Serial.begin(115200);
#endif
  debugln("Starting");

  // The default is (SDA, SCL) == (D2, D1) == (4, 5)
  // sht21.begin(SDA, SCL);
  // For ESP-01 use (0, 2)

  Wire.begin(0, 2);
  sht21.begin(0, 2);
  debugln("SHT21 initialized");

  if (bmp.begin()) {
    debugln("BMP180 initialized");
  }

  WiFiManager wifiManager;
  wifiManager.setConfigPortalTimeout(STATION_TIMEOUT);
  wifiManager.autoConnect("meteostation");
  debugln("WiFi done, sending mdns query");

  m_mdns.Clear();
  struct mdns::Query m_query;
  strncpy(m_query.qname_buffer, QUESTION_SERVICE, MAX_MDNS_NAME_LEN);
  m_query.qtype = MDNS_TYPE_PTR;
  m_query.qclass = 1;
  m_query.unicast_response = 0;
  m_mdns.AddQuery(m_query);
  m_mdns.Send();
}

int measures = -3, total = 10, timeout = TIMEOUT, attempts = 3;
float t, h;

void loop() {
  if (measures < total && sht21.measure(t, h)) {
    if (measures >= 0) {
      debugln("Measure values");
      meteo.temperature += t;
      meteo.humidity += h;
      meteo.pressure += 0.00750061683 * bmp.readPressure();
    } else {
      debugln("Warmup measure");
    }
    measures++;
    delay(50);
  }

  m_mdns.loop();

  if (measures >= total && service.address != "" && service.port != 0) {
    debugln("Done with measuring");
    meteo.temperature /= total;
    meteo.humidity /= total;
    meteo.pressure /= total;

    do {
      debugln("Attempt to send a value");
      delay(100);
    } while (!request(service, meteo, timeout) && attempts-- > 0);

    debug("Timeout value now is ");
    debugln(timeout);

    if (timeout < 60) {
      timeout = TIMEOUT;
    }

    debug("Deep sleep for ");
    debugln(timeout);
    ESP.deepSleep(timeout * 1000 * 1000);
  }

  if (millis() > PROCESS_TIMEOUT * 1000) {
    debug("Deep sleep for ");
    debug(timeout);
    debugln(" because of not detected endpoint");
    ESP.deepSleep(TIMEOUT * 1000 * 1000);
  }
}