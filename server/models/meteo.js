const Sequelize = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class Meteo extends Sequelize.Model {
    associate() {

    }
  }

  Meteo.init({
    channel: {
      allowNull: false,
      type: Sequelize.STRING,
      validate: {
        isAlpha: true,
      },
    },
    temperature: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
      validate: {
        isFloat: true,
        max: 100, // what?
        min: -100, // WHAT?!
      },
    },
    humidity: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
      validate: {
        isFloat: true,
        max: 100,
        min: 0,
      },
    },
    pressure: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
      validate: {
        isFloat: true,
        max: 1000, // are we under the pressure yet?
        min: 0, // well... vaccuum...
      }
    },
  }, {
    sequelize,
    modelName: "Meteo",
    tableName: "meteos",
  });

  return Meteo;
};
