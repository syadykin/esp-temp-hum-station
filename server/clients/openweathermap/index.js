const axios = require("axios");
const winston = require("winston");

const key = "69c295cb424402c679d2082b24afe941";
const city = "689558";

const url = `https://api.openweathermap.org/data/2.5/weather?id=${ city }&APPID=${ key }&units=metric&lang=en`;
const channel = "outdoor";
const station = "http://nas.home:4321";

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.label({ label: '[meteo-openweathermap]' }),
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  transports: [
    new winston.transports.Console()
  ],
});

const request = async () => {
  try {
    const { data: { main: { temp: temperature, humidity, pressure } } } = await axios.get(url);
    const body = JSON.stringify({
      temperature,
      humidity,
      pressure: pressure * 0.750061683,
    });
    logger.log("info", `Obtained info: ${ JSON.stringify(body) }`);

    const service = `${ station }/${ channel }`;
    await axios.request({
      url: service,
      method: "post",
      data: body,
      headers: {
        "Content-Type": "application/json",
      },
    });
    logger.log("info", `Successfuly posted to ${ service }`);
  } catch (err) {
    logger.log("error", `Error: ${ err.message }`);
  }
}

request();
