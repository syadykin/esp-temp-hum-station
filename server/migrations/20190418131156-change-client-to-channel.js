module.exports = {
  up: (queryInterface) => queryInterface.renameColumn("meteos", "client", "channel"),
  down: (queryInterface) => queryInterface.renameColumn("meteos", "channel", "client"),
};
