module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.createTable("meteos", {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER,
    },
    client: {
      type: Sequelize.STRING,
    },
    temperature: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
    },
    humidity: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
    },
    pressure: {
      allowNull: true,
      type: Sequelize.DECIMAL(6,2),
    },
    createdAt: {
      allowNull: false,
      type: Sequelize.DATE
    },
    updatedAt: {
      allowNull: false,
      type: Sequelize.DATE
    }
  }),
  down: (queryInterface) => queryInterface.dropTable("meteos"),
};
