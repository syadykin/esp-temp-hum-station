const config = require("config");

const ENV = process.env["NODE_ENV"] || "development";
module.exports = {
  [ENV]: config.get("db"),
};
