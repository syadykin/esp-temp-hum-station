const mdns = require("mdns");
const express = require("express");
const bodyParser = require("body-parser");
const winston = require("winston");
const config = require("config");

const { Meteo, Sequelize } = require("./models");

const logger = winston.createLogger({
  format: winston.format.combine(
    winston.format.label({ label: '[meteo]' }),
    winston.format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    winston.format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
  ),
  transports: [
    new winston.transports.Console()
  ],
});

const app = express();
app.set("view engine", "pug");

app.use(bodyParser.json());
app.use("/.static", express.static("static"));

const every = 300;

app.post(/^\/(\w+)$/, (req, res) => {
  const channel = req.params[0];

  Meteo.create({ ...req.body, channel });

  const now = Math.floor(new Date().valueOf() / 1000);
  const next = Math.ceil(now / every) * every;
  const timeout = next - now;

  logger.log("info", `recv: ${ JSON.stringify(req.body) }, send: ${ timeout }`);
  res.status(200).send("" + timeout);
});

app.get(/^\/(\w+)(?:\/(\d+)(?:\.\.(\d+))?)?$/, async (req, res) => {
  const channel = req.params[0];
  const from = req.params[1] || 0;
  const to = req.params[2] || 0;

  res.format({
    "application/json": async () => {
      const attributes = [
        "temperature",
        "humidity",
        "pressure",
        "createdAt",
      ];

      let data = {};

      if (from == null && to == null) {
        data = await Meteo.findOne({
          attributes,
          where: {
            channel,
            createdAt: {
              [Sequelize.Op.between]: [ start, end ],
            },
          },
          order: [ [ "createdAt", "DESC" ] ],
        });
      } else {
        const [ today ] = new Date().toISOString().split("T");

        const start = new Date(+from * 1000 || `${ today }T00:00:00.000Z`);
        const end = new Date(+to * 1000 || `${ today }T23:59:59.000Z`);

        data = await Meteo.findAll({
          attributes,
          where: {
            channel,
            createdAt: {
              [Sequelize.Op.between]: [ start, end ],
            },
          },
          order: [ [ "createdAt", "DESC" ] ],
        });
      }
      res.status(200).json(data);
    },
    "text/html": () => {
      res.render("index");
    },
  });
});

const start = async () => {
  const port = config.get("port");
  const ad = mdns.createAdvertisement(mdns.tcp(config.get("service")), port);
  ad.start();

  app.listen(port, () => logger.log("info", "start listening"));
}

start();
