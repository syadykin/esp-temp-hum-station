window.addEventListener("load", () => {
  const end = new Date();
  end.setHours(23);
  end.setMinutes(59);
  end.setSeconds(59);
  end.setMilliseconds(999);

  const start = new Date();
  start.setHours(0);
  start.setMinutes(0);
  start.setSeconds(0);
  start.setMilliseconds(0);

  const dates = { start, end };

  const draw = (data) => {
    const svgWidth = 1000, svgHeight = 600;
    const margin = { top: 20, right: 20, bottom: 30, left: 120 };
    const width = svgWidth - margin.left - margin.right;
    const height = svgHeight - margin.top - margin.bottom;

    const timeX = d3.scaleTime().rangeRound([0, width]);
    const temperatureY = d3.scaleLinear().rangeRound([height, 0]);
    const humidityY = d3.scaleLinear().rangeRound([height, 0]);
    const pressureY = d3.scaleLinear().rangeRound([height, 0]);

    const temperatureLine = d3.line()
      .x(d => timeX(Date.parse(d.createdAt)))
      .y(d => temperatureY(d.temperature))
      .curve(d3.curveBasis);

    const humidityLine = d3.line()
      .x(d => timeX(Date.parse(d.createdAt)))
      .y(d => humidityY(d.humidity))
      .curve(d3.curveBasis);

    const pressureLine = d3.line()
      .x(d => timeX(Date.parse(d.createdAt)))
      .y(d => pressureY(d.pressure))
      .curve(d3.curveBasis);

    timeX.domain([ dates.start, dates.end ]);

    const temp = d3.extent(data, d => d.temperature);
    temperatureY.domain([temp[0] - 10, temp[1] + 10]);

    humidityY.domain([0, 100])

    const press = d3.extent(data, d => d.pressure);
    pressureY.domain([Math.min(720, press[0]), Math.max(press[1], 780)]);

    const children = document.getElementById('graph').children;
    for (let i = 0, ilen = children.length; i < ilen; i++) {
      children[i].remove();
    }

    const root = d3.select("#graph")
      .attr("height", svgHeight)
      .attr("width", svgWidth);

    const group = root.append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    group.append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(timeX));

    group.append("g")
      .attr("transform", "translate(0,0)")
      .call(d3.axisLeft(temperatureY))
      .append("text")
      .attr("fill", "green")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Temperature");

    group.append("g")
      .attr("transform", "translate(-40,0)")
      .call(d3.axisLeft(humidityY))
      .append("text")
      .attr("fill", "blue")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Humidity");

    group.append("g")
      .attr("transform", "translate(-80,0)")
      .call(d3.axisLeft(pressureY))
      .append("text")
      .attr("fill", "red")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", "0.71em")
      .attr("text-anchor", "end")
      .text("Pressure");

    group.append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", "green")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1)
      .attr("d", temperatureLine);

    group.append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", "blue")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1)
      .attr("d", humidityLine);

    group.append("path")
      .datum(data)
      .attr("fill", "none")
      .attr("stroke", "red")
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1)
      .attr("d", pressureLine);
  }

  document.getElementById("start").value = dates.start.toISOString().split('T').shift();
  document.getElementById("end").value = dates.end.toISOString().split('T').shift();

  const load = () => {
    const [ , path ] = location.pathname.split("/");
    const url = `/${ path }/${ Math.floor(dates.start.valueOf() / 1000) }..${ Math.floor(dates.end.valueOf() / 1000) }`;
    fetch(url).then((res) => res.json()).then(draw);
  };

  onChange = (type) => ([ date ]) => {
    dates[type] = date;
    load();
  }

  flatpickr("#start", {
    onChange: onChange("start"),
  });

  flatpickr("#end", {
    onChange: onChange("end"),
  });

  load();
  setInterval(load, 5 * 60 * 1000);
});
